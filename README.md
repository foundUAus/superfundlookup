# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)


## Installation

You can install the package via composer:

```bash
composer require foundu/super-fund-lookup
```

## Usage

``` php
$superfundLookup = foundU\SuperFundLookup::init(); 

$superfundLookup = foundU\SuperFundLookup::init($guid);
```


### How do I get set up? ###

Add `SUPER_FUND_LOOKUP_GUID` to your environment file, or pass as an argument to init()

* Dependencies
    - Guzzle
    - SimpleXml ext
    - JSON ext

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
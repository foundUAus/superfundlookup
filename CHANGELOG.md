# Changelog

## 1.0.3 - 2020-11-20

- Handle SimpleXML error when service not giving valid input
    - Add ServiceOfflineOrUnresponsive Exception
    - TODO add result from response as contextual info in exception

## 1.0.2 - 2020-11-20

- Fix return of a single result

## 1.0.1 - 2020-11-18

- Fix issue when lookup service returned a different structure when returning 1 result compared to more than 1 result

## 1.0.0 - 2020-11-18

- Initial release

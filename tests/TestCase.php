<?php

namespace foundU\SuperFundLookup\Tests;

use foundU\SuperFundLookup\ServiceProviders\SuperFundLookupServiceProvider;

class TestCase extends \PHPUnit\Framework\TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        // additional setup
    }

    /**
     * @param $app
     * @return string[]
     */
    protected function getPackageProviders($app)
    {
        return [
            SuperFundLookupServiceProvider::class
        ];
    }

    /**
     * @param $app
     */
    protected function getEnvironmentSetUp($app)
    {
        // perform environment setup
    }
}
<?php

namespace foundU\SuperFundLookup\Exceptions;

use Exception;

class MissingGuidException extends Exception
{
    protected $message = 'There is no GUID set for Superannuation Lookup.';
}
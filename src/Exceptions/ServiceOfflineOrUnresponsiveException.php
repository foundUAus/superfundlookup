<?php

namespace foundU\SuperFundLookup\Exceptions;

use Exception;

class ServiceOfflineOrUnresponsiveException extends Exception
{
    // TODO add contextual info - response given from request
}

<?php

namespace foundU\SuperFundLookup\Exceptions;

use Exception;
use Throwable;

class SearchMethodNotSetupException extends Exception
{
    protected $message = 'The search method specified is not setup.';

    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);

        if ($message) {
            $this->message = $this->message . ' ' . $message;
        }
    }
}
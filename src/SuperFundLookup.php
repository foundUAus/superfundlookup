<?php

namespace foundU\SuperFundLookup;

use foundU\SuperFundLookup\Exceptions\InvalidAbnNumberException;
use foundU\SuperFundLookup\Exceptions\MissingGuidException;
use foundU\SuperFundLookup\Exceptions\SearchMethodNotSetupException;
use foundU\SuperFundLookup\Exceptions\ServiceOfflineOrUnresponsiveException;
use foundU\SuperFundLookup\Exceptions\UnrecognisedGuidException;
use GuzzleHttp\Client;
use Illuminate\Support\Arr;

/**
 * Class SuperFundLookup
 * @package foundU\SuperFundLookup
 */
class SuperFundLookup
{
    /**
     * Method to run searches against on each request
     *
     * When setting method in here, there must be a function existing with the same name in this class
     * and on the super fund lookup web service
     *
     * @see https://superfundlookup.gov.au/xmlsearch/SflXmlSearch.asmx
     *
     * @var string $methodForSearching
     */
    protected $methodForSearching = 'SearchByProduct';

    /**
     * @var SuperFundLookup $instance
     */
    private static $instance;

    /**
     * GUID to authenticate to the web service
     *
     * @var $guid string
     */
    protected $guid;

    /**
     * Bae URI requests will be submitted to
     *
     * @var string $baseUri
     */
    protected $baseUri = 'https://superfundlookup.gov.au/xmlsearch/SflXmlSearch.asmx/';

    /**
     * Guzzle Client
     *
     * @var $client Client
     */
    protected $client;

    /**
     * Search query
     *
     * @var null|string $queryString
     */
    protected $queryString = null;

    /**
     * Active funds only Yes/No
     *
     * @in_array Y,N
     *
     * @var string $activeOnly
     */
    protected $activeOnly = 'Y';

    /**
     * Limit of results to return that accept limit param
     *
     * @var int $limit
     */
    protected $limit = 20;

    /**
     * @var null|int $responseStatusCode
     */
    protected $responseStatusCode = null;

    /**
     * Response headers from request
     *
     * @var string[][] $responseHeaders
     */
    protected $responseHeaders = [];

    /**
     * SuperFundLookup constructor.
     * @param $guid
     */
    private function __construct($guid)
    {
        $this->guid = $guid;
    }

    /**
     * Initialise a new instance or return current instance of class
     *
     * Accepts a guid to override the env set guid if required
     *
     * @param null $guid
     * @return SuperFundLookup|static
     */
    public static function init($guid = null): SuperFundLookup
    {
        if (!$guid) {
            $guid = env('SUPER_FUND_LOOKUP_GUID');
        }

        if (!self::$instance) {
            self::$instance = new static($guid);
        }

        return self::$instance;
    }

    /**
     * @param string $methodForSearching
     */
    public function setMethodForSearching(string $methodForSearching): void
    {
        $this->methodForSearching = $methodForSearching;
    }

    /**
     * @return string[][]
     */
    public function getResponseHeaders(): array
    {
        return $this->responseHeaders;
    }

    /**
     * @return int|null
     */
    public function getResponseStatusCode(): ?int
    {
        return $this->responseStatusCode;
    }

    /**
     * @param $queryString
     * @param bool $activeOnly
     * @return array
     * @throws InvalidAbnNumberException
     * @throws MissingGuidException
     * @throws UnrecognisedGuidException
     * @throws SearchMethodNotSetupException
     * @throws ServiceOfflineOrUnresponsiveException
     */
    public function search($queryString, bool $activeOnly = true): array
    {
        if (!$this->guid) {
            throw new MissingGuidException();
        }

        $this->queryString = urlencode($queryString);
        $this->activeOnly = $activeOnly ? 'Y' : 'N';

        $this->client = new Client([
            'base_uri' => $this->baseUri,
            'timeout' => 15,
        ]);

        if (!method_exists($this, $this->methodForSearching)) {
            throw new SearchMethodNotSetupException('Search Method: ' . $this->methodForSearching);
        }
        $params = $this->{$this->methodForSearching}();
        $responseArray = $this->makeSearchRequest($this->methodForSearching, $params);
        if (!is_array($responseArray)) {
            return [];
        }
        // Check for any exceptions returned
        if ($exception = Arr::get($responseArray, 'Response.Exception')) {
            $code = $exception['Code'];
            $message = $exception['Description'];
            switch (strtoupper($code)) {
                case 'SEARCH':
                    // Nothing found so just empty it
                    $responseArray = [];
                    break;
                case 'WEBSERVICES':
                    throw new UnrecognisedGuidException($message);
                case 'NONSUPER':
                    throw new InvalidAbnNumberException($message);
                case 'OFFLINE_UNRESPONSIVE':
                    throw new ServiceOfflineOrUnresponsiveException($message);
            }
        }

        return $this->formatResponse($responseArray);
    }

    /**
     * Formats the response readable by application
     * Can be overridden by extending this class
     *
     * @param $response
     * @return array
     */
    protected function formatResponse($response): array
    {
        $results = [];

        switch ($this->methodForSearching) {
            case 'SearchByProduct':
                $matchingFunds = Arr::get($response, 'Response.MatchingFundProducts.MatchingFundProduct', []);
                $numberOfRecords = Arr::get($response, 'Response.MatchingFundProducts.NumberOfRecords', 0);
                // Response back if only 1 match is different when multiple matches return
                if ($numberOfRecords == 1) {
                    // Return an array of results to match multiple result response from api
                    return [
                        [
                            'usi' => $matchingFunds['USI'],
                            'productName' => $matchingFunds['ProductName'],
                            'abn' => $matchingFunds['ABN'],
                        ]
                    ];
                }
                $results = array_map(function ($row) {
                    return [
                        'usi' => $row['USI'],
                        'productName' => $row['ProductName'],
                        'abn' => $row['ABN'],
                    ];
                }, $matchingFunds);
                break;
        }

        return $results;
    }

    /**
     * Perform search request on the endpoint and return array of results
     *
     * @param $method
     * @param $params
     * @return array|null
     */
    private function makeSearchRequest($method, $params): ?array
    {
        $response = $this->client->get($method, [
            'query' => $params,
        ]);

        $this->responseStatusCode = $response->getStatusCode();
        $responseBody = $response->getBody();
        $this->responseHeaders = $response->getHeaders();

        return $this->convertXmlToArray($responseBody->getContents());
    }

    /**
     * LATEST: Same as SearchByABN but returns USI information where it exists
     *
     * @return array
     */
    private function SearchByABN2015(): array
    {
        return [
            'abn' > $this->queryString,
            'guid' => $this->guid,
        ];
    }

    /**
     * Search By ABN e.g. 19415776361
     *
     * @return array
     */
    private function SearchByABN(): array
    {
        return [
            'abn' > $this->queryString,
            'guid' => $this->guid,
        ];
    }

    /**
     * LATEST: Same as SearchByName201908 but accepts maxSearchResults to limit the number of records returned
     *
     * @return array
     */
    protected function SearchByName201911(): array
    {
        return [
            'name' => $this->queryString,
            'guid' => $this->guid,
            'activeFundsOnly' => $this->activeOnly,
            'maxSearchResults' => $this->limit,
        ];
    }

    /**
     * LATEST: Same as SearchByName but returns additional fields Fund Type Code and Description
     *
     * @return array
     */
    private function SearchByName201908(): array
    {
        return [
            'name' => $this->queryString,
            'guid' => $this->guid,
            'activeFundsOnly' => $this->activeOnly,
            'maxSearchResults' => $this->limit,
        ];
    }

    /**
     * Search By Name
     *
     * @return array
     */
    private function SearchByName(): array
    {
        return [
            'name' => $this->queryString,
            'guid' => $this->guid,
            'activeFundsOnly' => $this->activeOnly,
        ];
    }

    /**
     * Search By Product
     *
     * @return array
     */
    private function SearchByProduct(): array
    {
        return [
            'product' => $this->queryString,
            'guid' => $this->guid,
        ];
    }


    /**
     * Convert XML response to a more readable array response
     *
     * @param $results
     * @return array|null
     */
    protected function convertXmlToArray($results): ?array
    {
        try {
            $xml = simplexml_load_string($results, 'SimpleXMLElement', LIBXML_NOCDATA);
        } catch (\Exception $exception) {
            // Response isnt valid xml, service could be offline
            return [
                'Response' => [
                    'Exception' => [
                        'Code' => 'OFFLINE_UNRESPONSIVE',
                        'Description' => 'Service currently offline or invalid response given',
                        'Result' => $results
                    ]
                ]
            ];
        }
        return json_decode(json_encode($xml), true);
    }
}
